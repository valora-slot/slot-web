import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreatePlanningTemplateComponent } from './views/create-planning-template/create-planning-template.component';
import { PageNotFoundComponent } from './views/page-not-found/page-not-found.component';
import { PlanningTemplateComponent } from './views/planning-template/planning-template.component';
import { PlanningTemplatesComponent } from './views/planning-templates/planning-templates.component';
import { PlanningsComponent } from './views/plannings/plannings.component';
import { SchedulerComponent } from './views/scheduler/scheduler.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'scheduler',
    pathMatch: 'full'
  },
  {
    path: 'scheduler',
    component: SchedulerComponent
  },
  {
    path: 'plannings',
    component: PlanningsComponent
  },
  {
    path: 'planning/:id',
    component: PlanningTemplatesComponent
  },
  {
    path: 'planning/:id/template/new',
    component: CreatePlanningTemplateComponent
  },
  {
    path: 'template/:id',
    component: PlanningTemplateComponent
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
