import { registerLocaleData } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import localeFr from '@angular/common/locales/fr';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { frLocale } from 'ngx-bootstrap/locale';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InternationalizationModule } from './internationalization/internationalization.module';
import { ConfigurationService } from './services/configuration.service';
import { CreatePlanningTemplateComponent } from './views/create-planning-template/create-planning-template.component';
import { NavbarComponent } from './views/navbar/navbar.component';
import { PageNotFoundComponent } from './views/page-not-found/page-not-found.component';
import { PlanningTemplateComponent } from './views/planning-template/planning-template.component';
import { PlanningTemplatesComponent } from './views/planning-templates/planning-templates.component';
import { PlanningsComponent } from './views/plannings/plannings.component';
import { SchedulerComponent } from './views/scheduler/scheduler.component';

registerLocaleData(localeFr, 'fr');
defineLocale('fr', frLocale);

// AoT requires an exported function for factories
export function createTranslateLoader(http: HttpClient) {
  // TODO delete @ngx-translate/http-loader dependency and create a loader like ConfigurationService to avoid HttpClient
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

const configurationInit = (configurationService: ConfigurationService) => {
  return () => configurationService.init();
};

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SchedulerComponent,
    PageNotFoundComponent,
    PlanningsComponent,
    PlanningTemplatesComponent,
    CreatePlanningTemplateComponent,
    PlanningTemplateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    PaginationModule.forRoot(),
    PopoverModule.forRoot(),
    ModalModule.forRoot(),
    TimepickerModule.forRoot(),
    InternationalizationModule
  ],
  providers: [
    ConfigurationService,
    {
      provide: APP_INITIALIZER,
      useFactory: configurationInit,
      multi: true,
      deps: [ConfigurationService]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
