import { PaginatedResponse } from './paginated-response';
import { PlanningTemplateResponse } from './planning-template-response';

export class PlanningTemplateResponses extends PaginatedResponse {
  '_embedded': {
    planningTemplateResponseList: PlanningTemplateResponse[];
  };
}
