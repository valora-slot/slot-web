import { PaginatedResponse } from './paginated-response';
import { SlotTemplateResponse } from './slot-template-response';

export class SlotTemplateResponses extends PaginatedResponse {
  '_embedded': {
    slotTemplateResponseList: SlotTemplateResponse[];
  };
}
