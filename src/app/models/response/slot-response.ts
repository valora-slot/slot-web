import { Resource } from './resource';

export class SlotResponse extends Resource {
  id: string;
  startDateTime: Date;
  endDateTime: Date;
  numberOfReservations: number;
  maxCapacity: number;
  slotTemplateId: string;
  planningId: string;
}
