import { Resource } from './resource';

export class PlanningTemplateResponse extends Resource {
  id: string;
  startDateTime: Date;
  endDateTime: Date;
  planningId: string;
}
