import { Resource } from './resource';

export class BookableResourceResponse extends Resource {
  id: string;
  label: string;
}
