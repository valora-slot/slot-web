import { PlanningResponse } from './planning-response';

export class PlanningResponses {
  '_embedded': {
    planningResponseList: PlanningResponse[];
  };
}
