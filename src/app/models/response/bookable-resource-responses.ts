import { BookableResourceResponse } from './bookable-resource-response';

export class BookableResourceResponses {
  '_embedded': {
    bookableResourceResponseList: BookableResourceResponse[];
  };
}
