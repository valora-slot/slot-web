export abstract class PaginatedResponse {
  page: {
    size: number;
    totalElements: number;
    totalPages: number;
    number: number;
  };
}
