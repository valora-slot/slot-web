import { Resource } from './resource';

export class SlotTemplateResponse extends Resource {
  id: string;
  dayOfWeek: number;
  startTime: string;
  endTime: string;
  maxCapacity: number;
  planningTemplateId: string;
}
