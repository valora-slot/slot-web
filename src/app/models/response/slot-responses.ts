import { PaginatedResponse } from './paginated-response';
import { SlotResponse } from './slot-response';

export class SlotResponses extends PaginatedResponse {
  '_embedded': {
    slotResponseList: SlotResponse[];
  };
}
