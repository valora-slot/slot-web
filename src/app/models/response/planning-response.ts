import { Resource } from './resource';

export class PlanningResponse extends Resource {
  id: string;
  label: string;
  bookableResourceId: string;
}
