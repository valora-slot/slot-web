export class SlotTemplateCreateRequest {
  dayOfWeek: number;
  startTime: string;
  endTime: string;
  planningTemplateId: string;
  maxCapacity: number;
}
