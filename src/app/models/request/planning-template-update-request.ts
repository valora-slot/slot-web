export class PlanningTemplateUpdateRequest {
  startDateTime: Date;
  endDateTime: Date;
  planningId: string;
}
