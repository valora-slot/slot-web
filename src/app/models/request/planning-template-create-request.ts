export class PlanningTemplateCreateRequest {
  startDateTime: Date;
  endDateTime: Date;
  planningId: string;
}
