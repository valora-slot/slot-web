export class SlotTemplatePatchRequest {
  dayOfWeek: number;
  startTime: string;
  endTime: string;
}
