import { HttpParams } from '@angular/common/http';
import { Pagination } from './pagination';

export abstract class SearchCriteria {
  httpParams: HttpParams;

  constructor() {
    this.httpParams = new HttpParams();
  }

  setPagination(pagination: Pagination) {
    this.httpParams = this.httpParams.set('page', String(pagination.page - 1)).set('size', String(pagination.itemsPerPage));
  }

  toHttpParams(): HttpParams {
    return this.httpParams;
  }
}
