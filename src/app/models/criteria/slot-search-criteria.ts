import { SearchCriteria } from './search-criteria';

export class SlotSearchCriteria extends SearchCriteria {
  constructor() {
    super();
  }

  setBookableResourceId(bookableResourceId: string) {
    this.httpParams = this.httpParams.append('bookableResourceId', bookableResourceId);
  }

  setStartDateTimeRange(startDateTimeFrom: Date, startDateTimeTo: Date) {
    this.httpParams = this.httpParams
      .set('startDateTimeFrom', startDateTimeFrom.toISOString())
      .set('startDateTimeTo', startDateTimeTo.toISOString());
  }
}
