import { SearchCriteria } from './search-criteria';

export class SlotTemplateSearchCriteria extends SearchCriteria {
  constructor() {
    super();
  }

  setPlanningTemplateId(planningTemplateId: string) {
    this.httpParams = this.httpParams.append('planningTemplateId', planningTemplateId);
  }
}
