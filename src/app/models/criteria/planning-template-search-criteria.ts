import { SearchCriteria } from './search-criteria';

export class PlanningTemplateSearchCriteria extends SearchCriteria {
  constructor() {
    super();
  }

  setPlanningId(planningId: string) {
    this.httpParams = this.httpParams.append('planningId', planningId);
  }
}
