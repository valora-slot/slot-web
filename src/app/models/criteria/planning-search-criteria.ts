import { SearchCriteria } from './search-criteria';

export class PlanningSearchCriteria extends SearchCriteria {
  constructor() {
    super();
  }

  setBookableResourceId(bookableResourceId: string) {
    this.httpParams = this.httpParams.append('bookableResourceId', bookableResourceId);
  }
}
