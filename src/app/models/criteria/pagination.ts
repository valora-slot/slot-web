export class Pagination {
  /** limit number for page links in pager */
  maxSize: number;
  /** total number of items in all pages */
  totalItems: number;
  /** total number of pages */
  totalPages: number;
  /** current page */
  page: number;
  /** number of items per page */
  itemsPerPage: number;

  constructor() {
    this.page = 1;
    this.itemsPerPage = 10;
    this.maxSize = 3;
  }
}
