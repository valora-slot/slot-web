import { AbstractControl, ValidationErrors } from '@angular/forms';

export class DateRangeValidator {
  static startDateBeforeEndDate(control: AbstractControl): ValidationErrors {
    const ok = !control.get('endDateField').value || control.get('startDateField').value < control.get('endDateField').value;
    return ok ? null : { startDateAfterEndDate: { value: control.value } };
  }
}
