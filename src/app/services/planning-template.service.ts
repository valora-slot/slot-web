import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PlanningTemplateSearchCriteria } from '../models/criteria/planning-template-search-criteria';
import { PlanningTemplateCreateRequest } from '../models/request/planning-template-create-request';
import { PlanningTemplateUpdateRequest } from '../models/request/planning-template-update-request';
import { PlanningTemplateResponse } from '../models/response/planning-template-response';
import { PlanningTemplateResponses } from '../models/response/planning-template-responses';
import { ConfigurationService } from './configuration.service';

@Injectable({
  providedIn: 'root'
})
export class PlanningTemplateService {
  private slotServiceUrl: string;
  private resourceUrl = '/planning-templates';

  constructor(private configurationService: ConfigurationService, private http: HttpClient) {
    this.slotServiceUrl = this.configurationService.get('slotServiceUrl');
  }

  findById(id: string): Observable<PlanningTemplateResponse> {
    return this.http.get<PlanningTemplateResponse>(`${this.slotServiceUrl}${this.resourceUrl}/${id}`);
  }

  findAll(planningTemplateSearchCriteria: PlanningTemplateSearchCriteria): Observable<PlanningTemplateResponses> {
    return this.http.get<PlanningTemplateResponses>(`${this.slotServiceUrl}${this.resourceUrl}`, {
      params: planningTemplateSearchCriteria.toHttpParams()
    });
  }

  create(planningTemplateCreateRequest: PlanningTemplateCreateRequest): Observable<void> {
    return this.http.post<void>(`${this.slotServiceUrl}${this.resourceUrl}`, planningTemplateCreateRequest);
  }

  update(id: string, planningTemplateUpdateRequest: PlanningTemplateUpdateRequest): Observable<void> {
    return this.http.put<void>(`${this.slotServiceUrl}${this.resourceUrl}/${id}`, planningTemplateUpdateRequest);
  }

  delete(id: string): Observable<void> {
    return this.http.delete<void>(`${this.slotServiceUrl}${this.resourceUrl}/${id}`);
  }
}
