import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged, filter } from 'rxjs/operators';
import { BookableResourceResponse } from '../models/response/bookable-resource-response';
import { BookableResourceResponses } from '../models/response/bookable-resource-responses';
import { ConfigurationService } from './configuration.service';

@Injectable({
  providedIn: 'root'
})
export class BookableResourceService {
  private slotServiceUrl: string;
  private resourceUrl = '/bookable-resources';

  private currentBookableResourceId: BehaviorSubject<string>;

  constructor(private configurationService: ConfigurationService, private http: HttpClient) {
    this.slotServiceUrl = this.configurationService.get('slotServiceUrl');
    this.currentBookableResourceId = new BehaviorSubject(null);
  }

  getCurrentBookableResourceId(): Observable<string> {
    return this.currentBookableResourceId.asObservable().pipe(
      filter(bookableResourceId => bookableResourceId !== null),
      distinctUntilChanged()
    );
  }

  setCurrentBookableResourceId(newValue: string): void {
    this.currentBookableResourceId.next(newValue);
  }

  findById(id: string): Observable<BookableResourceResponse> {
    return this.http.get<BookableResourceResponse>(`${this.slotServiceUrl}${this.resourceUrl}/${id}`);
  }

  findAll(): Observable<BookableResourceResponses> {
    return this.http.get<BookableResourceResponses>(`${this.slotServiceUrl}${this.resourceUrl}`);
  }
}
