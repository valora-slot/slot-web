import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PlanningSearchCriteria } from '../models/criteria/planning-search-criteria';
import { PlanningResponse } from '../models/response/planning-response';
import { PlanningResponses } from '../models/response/planning-responses';
import { ConfigurationService } from './configuration.service';

@Injectable({
  providedIn: 'root'
})
export class PlanningService {
  private slotServiceUrl: string;
  private resourceUrl = '/plannings';

  constructor(private configurationService: ConfigurationService, private http: HttpClient) {
    this.slotServiceUrl = this.configurationService.get('slotServiceUrl');
  }

  findById(id: string): Observable<PlanningResponse> {
    return this.http.get<PlanningResponse>(`${this.slotServiceUrl}${this.resourceUrl}/${id}`);
  }

  findAll(planningSearchCriteria: PlanningSearchCriteria): Observable<PlanningResponses> {
    return this.http.get<PlanningResponses>(`${this.slotServiceUrl}${this.resourceUrl}`, { params: planningSearchCriteria.toHttpParams() });
  }
}
