import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SlotTemplateSearchCriteria } from '../models/criteria/slot-template-search-criteria';
import { SlotTemplateCreateRequest } from '../models/request/slot-template-create-request';
import { SlotTemplatePatchRequest } from '../models/request/slot-template-patch-request';
import { SlotTemplateResponse } from '../models/response/slot-template-response';
import { SlotTemplateResponses } from '../models/response/slot-template-responses';
import { ConfigurationService } from './configuration.service';

@Injectable({
  providedIn: 'root'
})
export class SlotTemplateService {
  private slotServiceUrl: string;
  private resourceUrl = '/slot-templates';

  constructor(private configurationService: ConfigurationService, private http: HttpClient) {
    this.slotServiceUrl = this.configurationService.get('slotServiceUrl');
  }

  findById(id: string): Observable<SlotTemplateResponse> {
    return this.http.get<SlotTemplateResponse>(`${this.slotServiceUrl}${this.resourceUrl}/${id}`);
  }

  findAll(slotTemplateSearchCriteria: SlotTemplateSearchCriteria): Observable<SlotTemplateResponses> {
    return this.http.get<SlotTemplateResponses>(`${this.slotServiceUrl}${this.resourceUrl}`, {
      params: slotTemplateSearchCriteria.toHttpParams()
    });
  }

  create(slotTemplateCreateRequest: SlotTemplateCreateRequest): Observable<void> {
    return this.http.post<void>(`${this.slotServiceUrl}${this.resourceUrl}`, slotTemplateCreateRequest);
  }

  patch(id: string, slotTemplatePatchRequest: SlotTemplatePatchRequest): Observable<void> {
    return this.http.patch<void>(`${this.slotServiceUrl}${this.resourceUrl}/${id}`, slotTemplatePatchRequest);
  }

  delete(id: string): Observable<void> {
    return this.http.delete<void>(`${this.slotServiceUrl}${this.resourceUrl}/${id}`);
  }
}
