import { Injectable } from '@angular/core';

@Injectable()
export class ConfigurationService {
  private settings: any;

  init(): Promise<any> {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.open('GET', '/assets/configuration.json');

      xhr.addEventListener('readystatechange', () => {
        if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
          this.settings = JSON.parse(xhr.responseText);
          resolve(this.settings);
        } else if (xhr.readyState === XMLHttpRequest.DONE) {
          reject();
        }
      });

      xhr.send(null);
    });
  }

  get(key: string) {
    return this.settings[key];
  }
}
