import { TestBed } from '@angular/core/testing';
import { PlanningTemplateService } from './planning-template.service';

describe('PlanningTemplateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlanningTemplateService = TestBed.get(PlanningTemplateService);
    expect(service).toBeTruthy();
  });
});
