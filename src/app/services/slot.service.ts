import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SlotSearchCriteria } from '../models/criteria/slot-search-criteria';
import { SlotResponses } from '../models/response/slot-responses';
import { ConfigurationService } from './configuration.service';

@Injectable({
  providedIn: 'root'
})
export class SlotService {
  private slotServiceUrl: string;
  private resourceUrl = '/slots';

  constructor(private configurationService: ConfigurationService, private http: HttpClient) {
    this.slotServiceUrl = this.configurationService.get('slotServiceUrl');
  }

  findAll(slotSearchCriteria: SlotSearchCriteria): Observable<SlotResponses> {
    return this.http.get<SlotResponses>(`${this.slotServiceUrl}${this.resourceUrl}`, {
      params: slotSearchCriteria.toHttpParams()
    });
  }
}
