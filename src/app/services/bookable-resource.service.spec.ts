import { TestBed } from '@angular/core/testing';
import { BookableResourceService } from './bookable-resource.service';

describe('BookableResourceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BookableResourceService = TestBed.get(BookableResourceService);
    expect(service).toBeTruthy();
  });
});
