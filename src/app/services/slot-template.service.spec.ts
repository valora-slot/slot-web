import { TestBed } from '@angular/core/testing';
import { SlotTemplateService } from './slot-template.service';

describe('SlotTemplateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SlotTemplateService = TestBed.get(SlotTemplateService);
    expect(service).toBeTruthy();
  });
});
