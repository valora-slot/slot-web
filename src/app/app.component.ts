import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  // The onLangChange subscription to update the component if the language change.
  onLangChange: Subscription = undefined;

  constructor(private translateService: TranslateService, private elementRef: ElementRef, private bsLocaleService: BsLocaleService) {}

  ngOnInit() {
    this.translateService.addLangs(['en', 'fr']);

    // this language will be used as a fallback when a translation isn't found in the current language
    this.translateService.setDefaultLang('en');

    // the lang to use, if the lang isn't available, it will use the current loader to get them
    const browserLang = this.translateService.getBrowserLang();
    const language = browserLang.match(/en|fr/) ? browserLang : 'en';
    this.translateService.use(language);
    this.bsLocaleService.use(language);

    this.updateLanguage();
    this.onLangChange = this.translateService.onLangChange.subscribe(() => {
      this.updateLanguage();
    });
  }

  ngOnDestroy() {
    if (this.onLangChange !== undefined) {
      this.onLangChange.unsubscribe();
    }
  }

  /**
   * Update the language in the lang attribute of the html element and datepicker language.
   */
  updateLanguage(): void {
    const lang = document.createAttribute('lang');
    lang.value = this.translateService.currentLang;
    this.bsLocaleService.use(lang.value);
    this.elementRef.nativeElement.parentElement.parentElement.attributes.setNamedItem(lang);
  }
}
