import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LocaleDateRangePipe } from './pipes/locale-date-range.pipe';
import { LocaleDatePipe } from './pipes/locale-date.pipe';

@NgModule({
  declarations: [LocaleDateRangePipe, LocaleDatePipe],
  imports: [CommonModule],
  exports: [LocaleDateRangePipe, LocaleDatePipe]
})
export class InternationalizationModule {}
