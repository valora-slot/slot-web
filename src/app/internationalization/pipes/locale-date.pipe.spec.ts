import { TestBed } from '@angular/core/testing';
import { LocaleDatePipe } from './locale-date.pipe';

describe('LocaleDatePipe', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('create an instance', () => {
    const pipe: LocaleDatePipe = TestBed.get(LocaleDatePipe);
    expect(pipe).toBeTruthy();
  });
});
