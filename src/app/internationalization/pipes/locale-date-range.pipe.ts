import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'dateRange',
  pure: false
})
export class LocaleDateRangePipe implements PipeTransform {
  private formattedValue: string | null;
  private lastBeginDate: Date;
  private lastEndDate: Date;
  private lastLang: string;

  constructor(private translateService: TranslateService) {}

  transform(range: Date[], pattern?: string): string {
    if (!range || range.length !== 2) {
      console.error('Invalid range for dateRange pipe');
      return 'Range error';
    }

    const currentLang = this.translateService.currentLang;
    const beginDate = range[0];
    const endDate = range[1];

    // if we ask another time for the same value & locale, return the last value
    if (beginDate === this.lastBeginDate && endDate === this.lastEndDate && currentLang === this.lastLang) {
      return this.formattedValue;
    }

    if (beginDate == null || endDate == null) {
      this.formattedValue = null;
    } else if (!pattern || pattern === 'MMMM yyyy') {
      if (beginDate.getFullYear() === endDate.getFullYear() && beginDate.getMonth() === endDate.getMonth()) {
        this.formattedValue = new DatePipe(currentLang).transform(beginDate, 'MMMM yyyy');
      } else if (beginDate.getFullYear() === endDate.getFullYear()) {
        this.formattedValue =
          new DatePipe(currentLang).transform(beginDate, 'MMMM') + ' - ' + new DatePipe(currentLang).transform(endDate, 'MMMM yyyy');
      } else {
        this.formattedValue =
          new DatePipe(currentLang).transform(beginDate, 'MMMM yyyy') + ' - ' + new DatePipe(currentLang).transform(endDate, 'MMMM yyyy');
      }
    } else {
      const beginWeek = new DatePipe(currentLang).transform(beginDate, 'w');
      const endWeek = new DatePipe(currentLang).transform(endDate, 'w');
      if (beginWeek === endWeek) {
        this.formattedValue = beginWeek;
      } else {
        this.formattedValue = beginWeek + ' - ' + endWeek;
      }
    }

    this.lastBeginDate = beginDate;
    this.lastEndDate = endDate;
    this.lastLang = currentLang;

    return this.formattedValue;
  }
}
