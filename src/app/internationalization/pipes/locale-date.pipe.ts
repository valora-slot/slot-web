import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'date',
  pure: false
})
export class LocaleDatePipe implements PipeTransform {
  private value: string | null;
  private lastValue: any;
  private lastLang: string;

  constructor(private translateService: TranslateService) {}

  transform(value: any, pattern?: string): any {
    const currentLang = this.translateService.currentLang;

    // if we ask another time for the same value & locale, return the last value
    if (value === this.lastValue && currentLang === this.lastLang) {
      return this.value;
    }

    this.value = new DatePipe(currentLang).transform(value, pattern);
    this.lastValue = value;
    this.lastLang = currentLang;

    return this.value;
  }
}
