import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { Pagination } from 'src/app/models/criteria/pagination';
import { PlanningTemplateSearchCriteria } from 'src/app/models/criteria/planning-template-search-criteria';
import { PlanningResponse } from 'src/app/models/response/planning-response';
import { PlanningTemplateResponse } from 'src/app/models/response/planning-template-response';
import { PlanningTemplateService } from 'src/app/services/planning-template.service';
import { PlanningService } from 'src/app/services/planning.service';

@Component({
  selector: 'app-planning-templates',
  templateUrl: './planning-templates.component.html',
  styleUrls: ['./planning-templates.component.scss']
})
export class PlanningTemplatesComponent implements OnInit {
  planning: PlanningResponse;
  planningTemplates: PlanningTemplateResponse[];

  paginationField: FormControl;
  pagination: Pagination;

  planningId: string;

  constructor(
    private planningService: PlanningService,
    private planningTemplateService: PlanningTemplateService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.planningId = this.route.snapshot.paramMap.get('id');

    this.planningService.findById(this.planningId).subscribe(planningResponse => {
      this.planning = planningResponse;
    });

    this.pagination = new Pagination();
    this.paginationField = this.formBuilder.control(this.pagination.page);
    this.paginationField.valueChanges.subscribe((page: number) => {
      this.pagination.page = page;

      const extras: NavigationExtras = {
        fragment: this.route.snapshot.fragment,
        queryParams: Object.assign({}, this.route.snapshot.queryParams),
        relativeTo: this.route
      };
      extras.queryParams.page = page ? page.toString() : '1';
      this.router.navigate([], extras);
    });

    this.route.queryParams.subscribe(queryParams => {
      if (queryParams.page) {
        const page = parseInt(queryParams.page, 10);
        if (this.paginationField.value !== page) {
          this.paginationField.setValue(page);
        }
      }
      this.loadTemplates();
    });
  }

  private loadTemplates() {
    const planningTemplateSearchCriteria = new PlanningTemplateSearchCriteria();
    planningTemplateSearchCriteria.setPlanningId(this.planningId);
    planningTemplateSearchCriteria.setPagination(this.pagination);
    this.planningTemplateService.findAll(planningTemplateSearchCriteria).subscribe(planningTemplateResponses => {
      this.planningTemplates = planningTemplateResponses._embedded.planningTemplateResponseList;
      this.pagination.totalItems = planningTemplateResponses.page.totalElements;
      this.pagination.totalPages = planningTemplateResponses.page.totalPages;
    });
  }
}
