import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PlanningTemplatesComponent } from './planning-templates.component';

describe('PlanningTemplatesComponent', () => {
  let component: PlanningTemplatesComponent;
  let fixture: ComponentFixture<PlanningTemplatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlanningTemplatesComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanningTemplatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
