import { Component, OnInit } from '@angular/core';
import { PlanningSearchCriteria } from 'src/app/models/criteria/planning-search-criteria';
import { PlanningResponse } from 'src/app/models/response/planning-response';
import { BookableResourceService } from 'src/app/services/bookable-resource.service';
import { PlanningService } from 'src/app/services/planning.service';

@Component({
  selector: 'app-plannings',
  templateUrl: './plannings.component.html',
  styleUrls: ['./plannings.component.scss']
})
export class PlanningsComponent implements OnInit {
  plannings: PlanningResponse[];

  constructor(private bookableResourceService: BookableResourceService, private planningService: PlanningService) {}

  ngOnInit() {
    this.bookableResourceService.getCurrentBookableResourceId().subscribe(currentBookableResourceId => {
      this.plannings = null;
      this.loadPlannings(currentBookableResourceId);
    });
  }

  private loadPlannings(bookableResourceId: string) {
    const planningSearchCriteria = new PlanningSearchCriteria();
    planningSearchCriteria.setBookableResourceId(bookableResourceId);

    this.planningService.findAll(planningSearchCriteria).subscribe(planningResponses => {
      this.plannings = planningResponses._embedded.planningResponseList;
    });
  }
}
