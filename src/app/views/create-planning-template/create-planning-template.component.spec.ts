import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CreatePlanningTemplateComponent } from './create-planning-template.component';

describe('CreatePlanningTemplateComponent', () => {
  let component: CreatePlanningTemplateComponent;
  let fixture: ComponentFixture<CreatePlanningTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreatePlanningTemplateComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePlanningTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
