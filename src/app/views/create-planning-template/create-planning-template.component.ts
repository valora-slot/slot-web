import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PlanningTemplateCreateRequest } from 'src/app/models/request/planning-template-create-request';
import { DateRangeValidator } from 'src/app/models/validator/date-range-validator';
import { PlanningTemplateService } from 'src/app/services/planning-template.service';

@Component({
  selector: 'app-create-planning-template',
  templateUrl: './create-planning-template.component.html',
  styleUrls: ['./create-planning-template.component.scss']
})
export class CreatePlanningTemplateComponent implements OnInit {
  saveForm: FormGroup;
  startDateField: FormControl;
  endDateField: FormControl;
  today: Date;

  private planningId: string;

  constructor(
    private planningTemplateService: PlanningTemplateService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.planningId = this.route.snapshot.paramMap.get('id');

    this.today = new Date();

    this.startDateField = this.formBuilder.control(null, [Validators.required]);
    this.endDateField = this.formBuilder.control(null);
    this.saveForm = this.formBuilder.group(
      {
        startDateField: this.startDateField,
        endDateField: this.endDateField
      },
      { validator: DateRangeValidator.startDateBeforeEndDate }
    );
  }

  save() {
    const planningTemplateCreateRequest = new PlanningTemplateCreateRequest();
    planningTemplateCreateRequest.startDateTime = this.startDateField.value;
    planningTemplateCreateRequest.endDateTime = this.endDateField.value;
    planningTemplateCreateRequest.planningId = this.planningId;

    this.planningTemplateService.create(planningTemplateCreateRequest).subscribe(success => {
      this.router.navigate(['planning', this.planningId]);
    });
  }
}
