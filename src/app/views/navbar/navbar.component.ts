import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { BookableResourceResponse } from 'src/app/models/response/bookable-resource-response';
import { BookableResourceService } from 'src/app/services/bookable-resource.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  opened = false;

  languages: string[];
  selectLanguageForm: FormGroup;
  languageField: FormControl;

  bookableResources: BookableResourceResponse[];
  selectBookableResourceForm: FormGroup;
  bookableResourceField: FormControl;

  constructor(
    private translateService: TranslateService,
    private bookableResourceService: BookableResourceService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.languages = this.translateService.getLangs();

    this.languageField = this.formBuilder.control('');
    this.languageField.setValue(this.translateService.currentLang);
    this.languageField.valueChanges.subscribe(newValue => this.translateService.use(newValue));

    this.selectLanguageForm = this.formBuilder.group({
      languageField: this.languageField
    });

    this.bookableResourceField = this.formBuilder.control('');
    this.bookableResourceField.valueChanges.subscribe((newValue: string) => {
      this.bookableResourceService.setCurrentBookableResourceId(newValue);
    });

    this.selectBookableResourceForm = this.formBuilder.group({
      bookableResourceField: this.bookableResourceField
    });

    this.bookableResourceService.findAll().subscribe(bookableResourceResponses => {
      this.bookableResources = bookableResourceResponses._embedded.bookableResourceResponseList;
      if (this.bookableResources.length > 0) {
        this.bookableResourceField.setValue(this.bookableResources[0].id);
      }
    });
  }

  toggleNavbar() {
    this.opened = !this.opened;
  }
}
