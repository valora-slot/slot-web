import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, OnInit, Renderer2, TemplateRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { PopoverDirective } from 'ngx-bootstrap/popover/public_api';
import { SlotTemplateSearchCriteria } from 'src/app/models/criteria/slot-template-search-criteria';
import { PlanningTemplateUpdateRequest } from 'src/app/models/request/planning-template-update-request';
import { SlotTemplateCreateRequest } from 'src/app/models/request/slot-template-create-request';
import { SlotTemplatePatchRequest } from 'src/app/models/request/slot-template-patch-request';
import { SlotTemplateResponse } from 'src/app/models/response/slot-template-response';
import { DateRangeValidator } from 'src/app/models/validator/date-range-validator';
import { PlanningTemplateService } from 'src/app/services/planning-template.service';
import { SlotTemplateService } from 'src/app/services/slot-template.service';

class SlotTemplate {
  private slotTemplateResponse: SlotTemplateResponse;
  startDateTime: Date;
  endDateTime: Date;

  constructor(slotTemplateResponse: SlotTemplateResponse) {
    this.slotTemplateResponse = slotTemplateResponse;

    this.startDateTime = new Date();
    const splittedStartTime = this.slotTemplateResponse.startTime.split(':');
    this.startDateTime.setHours(+splittedStartTime[0], +splittedStartTime[1], +splittedStartTime[2], 0);

    this.endDateTime = new Date();
    const splittedEndTime = this.slotTemplateResponse.endTime.split(':');
    this.endDateTime.setHours(+splittedEndTime[0], +splittedEndTime[1], +splittedEndTime[2], 0);
  }

  get id() {
    return this.slotTemplateResponse.id;
  }

  get dayOfWeek() {
    return this.slotTemplateResponse.dayOfWeek;
  }

  get maxCapacity() {
    return this.slotTemplateResponse.maxCapacity;
  }
}

class SlotTemplates {
  slotTemplates: SlotTemplate[];

  constructor() {
    this.slotTemplates = [];
  }

  add(slotTemplate: SlotTemplate) {
    this.slotTemplates.push(slotTemplate);
  }
}

@Component({
  selector: 'app-planning-template',
  templateUrl: './planning-template.component.html',
  styleUrls: ['./planning-template.component.scss']
})
export class PlanningTemplateComponent implements OnInit, AfterViewInit {
  saveForm: FormGroup;
  startDateField: FormControl;
  endDateField: FormControl;

  editSlotForm: FormGroup;
  editSlotStartTimeField: FormControl;
  editSlotEndTimeField: FormControl;

  timetableHours: Date[];
  timetableDays: Date[];
  timetableSlots: SlotTemplates[][];

  private createdSlotTemplates: SlotTemplate[];

  editSlotModal: BsModalRef;
  editedSlotTemplate: SlotTemplate;
  private openedPopover: PopoverDirective;
  private datePipe: DatePipe;
  private editedSlotTemplates: SlotTemplate[];

  confirmDeletionModal: BsModalRef;
  private deletedSlotTemplates: SlotTemplate[];

  private id: string;
  private planningId: string;
  private slotDurationInMinutes = 60;
  private numberOfDaysToShow = 7;
  private beginTimetableHour = 7;

  constructor(
    private planningTemplateService: PlanningTemplateService,
    private slotTemplateService: SlotTemplateService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private renderer: Renderer2
  ) {}

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');

    this.startDateField = this.formBuilder.control(null, [Validators.required]);
    this.endDateField = this.formBuilder.control(null);
    this.saveForm = this.formBuilder.group(
      {
        startDateField: this.startDateField,
        endDateField: this.endDateField
      },
      { validator: DateRangeValidator.startDateBeforeEndDate }
    );

    this.editSlotStartTimeField = this.formBuilder.control(null, [Validators.required]);
    this.editSlotEndTimeField = this.formBuilder.control(null, [Validators.required]);
    this.editSlotForm = this.formBuilder.group(
      {
        startDateField: this.editSlotStartTimeField,
        endDateField: this.editSlotEndTimeField
      },
      { validator: DateRangeValidator.startDateBeforeEndDate }
    );

    this.createdSlotTemplates = [];
    this.editedSlotTemplates = [];
    this.deletedSlotTemplates = [];
    this.datePipe = new DatePipe('en');

    this.planningTemplateService.findById(this.id).subscribe(planningTemplateResponse => {
      this.startDateField.setValue(new Date(planningTemplateResponse.startDateTime));
      if (planningTemplateResponse.endDateTime) {
        this.endDateField.setValue(new Date(planningTemplateResponse.endDateTime));
      }
      this.planningId = planningTemplateResponse.planningId;
    });

    this.generateTimetableHours();
    this.generateTimetableDays();
    this.generateTimetableSlots();

    const slotTemplateSearchCriteria = new SlotTemplateSearchCriteria();
    slotTemplateSearchCriteria.setPlanningTemplateId(this.id);
    this.slotTemplateService.findAll(slotTemplateSearchCriteria).subscribe(slotTemplateResponses => {
      this.generatePlanning(slotTemplateResponses._embedded.slotTemplateResponseList);
    });
  }

  ngAfterViewInit() {
    const element = this.renderer.selectRootElement('.begin-time');
    element.scrollIntoView();
  }

  private generateTimetableDays() {
    this.timetableDays = [];

    const day = new Date();
    day.setHours(0, 0, 0, 0);
    // Start on Monday (1), end on Sunday (0)
    day.setDate(day.getDate() - (day.getDay() - 1));

    for (let i = 0; i < this.numberOfDaysToShow; i++) {
      this.timetableDays.push(new Date(day.getTime()));
      day.setDate(day.getDate() + 1);
    }
  }

  private generateTimetableHours() {
    this.timetableHours = [];

    const slotBeginTime = new Date();
    slotBeginTime.setHours(0, 0, 0, 0);

    const tomorrow = new Date();
    tomorrow.setHours(0, 0, 0, 0);
    tomorrow.setDate(tomorrow.getDate() + 1);

    while (slotBeginTime < tomorrow) {
      this.timetableHours.push(new Date(slotBeginTime.getTime()));
      slotBeginTime.setMinutes(slotBeginTime.getMinutes() + this.slotDurationInMinutes);
    }
  }

  private generateTimetableSlots() {
    this.timetableSlots = [];

    for (let i = 0; i < this.timetableHours.length; i++) {
      this.timetableSlots[i] = [];
      for (let j = 0; j < this.timetableDays.length; j++) {
        this.timetableSlots[i][j] = new SlotTemplates();
      }
    }
  }

  private generatePlanning(slotTemplateResponses: SlotTemplateResponse[]) {
    slotTemplateResponses.forEach(slotTemplateResponse => {
      const dayIndex = slotTemplateResponse.dayOfWeek - 1;
      const slotTemplate = new SlotTemplate(slotTemplateResponse);
      const hourIndex = this.findTimetableSlotIndex(slotTemplate.startDateTime);
      this.timetableSlots[hourIndex][dayIndex].add(slotTemplate);
    });
  }

  private findTimetableSlotIndex(startDateTime: Date): number {
    const dateTime = startDateTime.getTime();
    for (let i = 0; i < this.timetableHours.length - 1; i++) {
      if (this.timetableHours[i].getTime() <= dateTime && dateTime < this.timetableHours[i + 1].getTime()) {
        return i;
      }
    }
    return this.timetableHours.length - 1;
  }

  isBeginTime(timetableHour: Date): boolean {
    return timetableHour.getHours() === this.beginTimetableHour;
  }

  getTopPercentage(timetableHour: Date, slotStartDateTime: Date): number {
    // Difference in ms / number of ms in 1s / number of s in 1min * 100% / this.slotDurationInMinutes
    return Math.floor((((slotStartDateTime.getTime() - timetableHour.getTime()) / 1000 / 60) * 100) / this.slotDurationInMinutes);
  }

  getHeightPercentage(slotStartDateTime: Date, slotEndDateTime: Date): number {
    // Difference in ms / number of ms in 1s / number of s in 1min * 100% / this.slotDurationInMinutes
    return Math.floor((((slotEndDateTime.getTime() - slotStartDateTime.getTime()) / 1000 / 60) * 100) / this.slotDurationInMinutes);
  }

  getWidthPercentage(numberOfItems: number): number {
    return Math.floor(100 / numberOfItems);
  }

  getLeftPercentage(numberOfItems: number, index: number): number {
    return this.getWidthPercentage(numberOfItems) * index;
  }

  openPopover(openedPopover: PopoverDirective) {
    this.openedPopover = openedPopover;
  }

  openEditSlotModal(template: TemplateRef<any>, slotTemplate: SlotTemplate) {
    this.openedPopover.hide();
    this.editedSlotTemplate = slotTemplate;
    this.findAndRemoveFromArray(this.editedSlotTemplates, slotTemplate);
    this.editSlotStartTimeField.setValue(slotTemplate.startDateTime);
    this.editSlotEndTimeField.setValue(slotTemplate.endDateTime);
    this.editSlotModal = this.modalService.show(template);
  }

  createSlot(dayIndex: number, hourIndex: number) {
    const slotTemplateResponse = new SlotTemplateResponse();
    slotTemplateResponse.dayOfWeek = dayIndex + 1;
    slotTemplateResponse.startTime = this.fromHourIndexToTime(hourIndex);
    slotTemplateResponse.endTime = this.fromHourIndexToTime(hourIndex + 1);
    slotTemplateResponse.maxCapacity = 10;
    slotTemplateResponse.planningTemplateId = this.id;
    const slotTemplate = new SlotTemplate(slotTemplateResponse);
    this.timetableSlots[hourIndex][dayIndex].slotTemplates.push(slotTemplate);
    this.createdSlotTemplates.push(slotTemplate);
  }

  private fromHourIndexToTime(hourIndex: number): string {
    const time = new Date();
    time.setHours(0, hourIndex * this.slotDurationInMinutes, 0, 0);
    return `${time.getHours()}:${time.getMinutes()}:00`;
  }

  editSlot() {
    this.editedSlotTemplate.startDateTime = this.editSlotStartTimeField.value;
    this.editedSlotTemplate.endDateTime = this.editSlotEndTimeField.value;
    if (this.editedSlotTemplate.id !== undefined) {
      this.editedSlotTemplates.push(this.editedSlotTemplate);
    }
    this.editSlotModal.hide();
  }

  deleteSlot(slotTemplates: SlotTemplate[], slotTemplate: SlotTemplate) {
    this.openedPopover.hide();
    this.findAndRemoveFromArray(slotTemplates, slotTemplate);
    if (slotTemplate.id === undefined) {
      this.findAndRemoveFromArray(this.createdSlotTemplates, slotTemplate);
    } else {
      this.findAndRemoveFromArray(this.editedSlotTemplates, slotTemplate);
      this.deletedSlotTemplates.push(slotTemplate);
    }
  }

  private findAndRemoveFromArray(array: any[], toRemove: any) {
    const index = array.indexOf(toRemove);
    if (index > -1) {
      array.splice(index, 1);
    }
  }

  save() {
    this.createdSlotTemplates.forEach(slotTemplate => {
      const slotTemplateCreateRequest = new SlotTemplateCreateRequest();
      slotTemplateCreateRequest.dayOfWeek = slotTemplate.dayOfWeek;
      slotTemplateCreateRequest.startTime = this.datePipe.transform(slotTemplate.startDateTime, 'HH:mm:SS');
      slotTemplateCreateRequest.endTime = this.datePipe.transform(slotTemplate.endDateTime, 'HH:mm:SS');
      slotTemplateCreateRequest.maxCapacity = slotTemplate.maxCapacity;
      slotTemplateCreateRequest.planningTemplateId = this.id;
      this.slotTemplateService.create(slotTemplateCreateRequest).subscribe();
    });

    this.editedSlotTemplates.forEach(slotTemplate => {
      const slotTemplatePatchRequest = new SlotTemplatePatchRequest();
      slotTemplatePatchRequest.startTime = this.datePipe.transform(slotTemplate.startDateTime, 'HH:mm:SS');
      slotTemplatePatchRequest.endTime = this.datePipe.transform(slotTemplate.endDateTime, 'HH:mm:SS');
      this.slotTemplateService.patch(slotTemplate.id, slotTemplatePatchRequest).subscribe();
    });

    this.deletedSlotTemplates.forEach(slotTemplate => {
      this.slotTemplateService.delete(slotTemplate.id).subscribe();
    });

    const planningTemplateUpdateRequest = new PlanningTemplateUpdateRequest();
    planningTemplateUpdateRequest.startDateTime = this.startDateField.value;
    planningTemplateUpdateRequest.endDateTime = this.endDateField.value;
    planningTemplateUpdateRequest.planningId = this.planningId;
    this.planningTemplateService.update(this.id, planningTemplateUpdateRequest).subscribe(success => {
      this.router.navigate(['planning', this.planningId]);
    });
  }

  delete(template: TemplateRef<any>) {
    this.confirmDeletionModal = this.modalService.show(template);
  }

  confirm() {
    this.confirmDeletionModal.hide();
    this.planningTemplateService.delete(this.id).subscribe(success => {
      this.router.navigate(['planning', this.planningId]);
    });
  }

  decline() {
    this.confirmDeletionModal.hide();
  }
}
