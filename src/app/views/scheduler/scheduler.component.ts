import { AfterViewChecked, Component, OnInit, Renderer2 } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { SlotSearchCriteria } from 'src/app/models/criteria/slot-search-criteria';
import { SlotResponse } from 'src/app/models/response/slot-response';
import { BookableResourceService } from 'src/app/services/bookable-resource.service';
import { SlotService } from 'src/app/services/slot.service';

class Slot {
  private slotResponse: SlotResponse;

  constructor(slotResponse: SlotResponse) {
    this.slotResponse = slotResponse;
    this.slotResponse.startDateTime = new Date(slotResponse.startDateTime);
    this.slotResponse.endDateTime = new Date(slotResponse.endDateTime);
  }

  get startDateTime() {
    return this.slotResponse.startDateTime;
  }

  get endDateTime() {
    return this.slotResponse.endDateTime;
  }

  get maxCapacity() {
    return this.slotResponse.maxCapacity;
  }

  get slotTemplateId() {
    return this.slotResponse.slotTemplateId;
  }

  get numberOfReservations() {
    return this.slotResponse.numberOfReservations;
  }
}

class Slots {
  slots: Slot[];
  hourStartDateTime: Date;

  constructor() {
    this.slots = [];
  }

  add(slot: Slot) {
    this.slots.push(slot);
  }
}

@Component({
  selector: 'app-scheduler',
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.scss']
})
export class SchedulerComponent implements OnInit, AfterViewChecked {
  selectedDate: string;

  now: Date;
  nowDayIndex: number;
  nowSlotIndex: number;
  beginDate: Date;
  endDate: Date;

  timetableHours: Date[];
  timetableDays: Date[];
  timetableSlots: Slots[][];

  private slotDurationInMinutes = 60;
  private numberOfDaysToShow = 4;
  private beginTimetableHour = 7;
  private firstDayOfScheduler: Date;
  firstDayOfSchedulerField: FormControl;

  private currentBookableResourceId: string;

  constructor(
    private formBuilder: FormBuilder,
    private renderer: Renderer2,
    private bookableResourceService: BookableResourceService,
    private slotService: SlotService
  ) {}

  ngOnInit() {
    this.bookableResourceService.getCurrentBookableResourceId().subscribe(currentBookableResourceId => {
      this.currentBookableResourceId = currentBookableResourceId;
      this.loadSlots();
    });

    this.now = new Date();
    this.nowDayIndex = null;
    this.nowSlotIndex = null;
    this.firstDayOfScheduler = new Date(this.now.getTime());
    this.firstDayOfScheduler.setHours(0, 0, 0, 0);
    this.firstDayOfSchedulerField = this.formBuilder.control(this.firstDayOfScheduler);
    this.firstDayOfSchedulerField.valueChanges.subscribe((newFirstDayOfPlanning: Date) => {
      this.firstDayOfScheduler = newFirstDayOfPlanning;
      this.generateTimetableDays();
      this.loadSlots();
    });

    this.generateTimetableHours();
    this.generateTimetableDays();
    this.generateTimetableSlots();
  }

  ngAfterViewChecked() {
    const element = this.renderer.selectRootElement('.begin-time');
    element.scrollIntoView();
  }

  isBeginTime(timetableHour: Date): boolean {
    return timetableHour.getHours() === this.beginTimetableHour;
  }

  goToToday() {
    const newFirstDayOfPlanning = new Date(this.now.getTime());
    newFirstDayOfPlanning.setHours(0, 0, 0, 0);
    this.firstDayOfScheduler.setTime(newFirstDayOfPlanning.getTime());
    this.generateTimetableDays();
    this.loadSlots();
  }

  previous() {
    this.changeDate(this.firstDayOfScheduler.getDate() - this.numberOfDaysToShow);
  }

  next() {
    this.changeDate(this.firstDayOfScheduler.getDate() + this.numberOfDaysToShow);
  }

  private changeDate(newDateDayOfTheMonth: number) {
    this.firstDayOfScheduler.setDate(newDateDayOfTheMonth);
    this.generateTimetableDays();
    this.loadSlots();
  }

  private generateTimetableHours() {
    this.timetableHours = [];

    const slotBeginTime = new Date(this.firstDayOfScheduler.getTime());

    const tomorrow = new Date(slotBeginTime.getTime());
    tomorrow.setDate(tomorrow.getDate() + 1);

    while (slotBeginTime < tomorrow) {
      this.timetableHours.push(new Date(slotBeginTime.getTime()));
      slotBeginTime.setMinutes(slotBeginTime.getMinutes() + this.slotDurationInMinutes);
    }
  }

  private generateTimetableDays() {
    this.timetableDays = [];

    const day = new Date(this.firstDayOfScheduler.getTime());

    for (let i = 0; i < this.numberOfDaysToShow; i++) {
      this.timetableDays.push(new Date(day.getTime()));
      day.setDate(day.getDate() + 1);
    }

    this.beginDate = new Date(this.timetableDays[0].getTime());
    this.endDate = new Date(this.timetableDays[this.timetableDays.length - 1].getTime());
    this.endDate.setHours(23, 59, 59, 999);
  }

  private generateTimetableSlots() {
    this.timetableSlots = [];

    this.timetableHours.forEach((hour, i) => {
      this.timetableSlots[i] = [];
      this.timetableDays.forEach((day, j) => {
        const date = new Date(hour.getTime());
        date.setDate(date.getDate() + j);
        this.timetableSlots[i][j] = new Slots();
        this.timetableSlots[i][j].hourStartDateTime = date;
      });
    });
  }

  private loadSlots() {
    if (this.currentBookableResourceId && this.beginDate && this.endDate) {
      const slotSearchCriteria = new SlotSearchCriteria();
      slotSearchCriteria.setBookableResourceId(this.currentBookableResourceId);
      slotSearchCriteria.setStartDateTimeRange(this.beginDate, this.endDate);

      this.slotService.findAll(slotSearchCriteria).subscribe(slotResponses => {
        this.generatePlanning(slotResponses._embedded.slotResponseList);
      });
    }
  }

  private generatePlanning(slotResponses: SlotResponse[]) {
    this.generateTimetableHours();
    this.generateTimetableSlots();

    const now = new Date();
    if (this.beginDate <= now && now <= this.endDate) {
      this.nowDayIndex = this.findTimetableDayIndex(now);
      this.nowSlotIndex = this.findTimetableSlotIndex(now, this.nowDayIndex);
    } else {
      this.nowDayIndex = null;
      this.nowSlotIndex = null;
    }

    slotResponses.forEach(slotResponse => {
      slotResponse.startDateTime = new Date(slotResponse.startDateTime);
      const dayIndex = this.findTimetableDayIndex(slotResponse.startDateTime);
      const slotIndex = this.findTimetableSlotIndex(slotResponse.startDateTime, dayIndex);
      this.timetableSlots[slotIndex][dayIndex].add(new Slot(slotResponse));
    });
  }

  private findTimetableDayIndex(date: Date): number {
    const dateTime = date.getTime();
    for (let i = 0; i < this.timetableDays.length - 1; i++) {
      if (this.timetableDays[i].getTime() <= dateTime && dateTime < this.timetableDays[i + 1].getTime()) {
        return i;
      }
    }
    return this.timetableDays.length - 1;
  }

  private findTimetableSlotIndex(date: Date, dayIndex: number): number {
    const dateTime = date.getTime();
    for (let i = 0; i < this.timetableHours.length - 1; i++) {
      if (
        this.timetableSlots[i][dayIndex].hourStartDateTime.getTime() <= dateTime &&
        dateTime < this.timetableSlots[i + 1][dayIndex].hourStartDateTime.getTime()
      ) {
        return i;
      }
    }
    return this.timetableHours.length - 1;
  }

  getTopPercentage(timetableHour: Date, slotStartDateTime: Date): number {
    // Difference in ms / number of ms in 1s / number of s in 1min * 100% / this.slotDurationInMinutes
    return Math.floor((((slotStartDateTime.getTime() - timetableHour.getTime()) / 1000 / 60) * 100) / this.slotDurationInMinutes);
  }

  getHeightPercentage(slotStartDateTime: Date, slotEndDateTime: Date): number {
    // Difference in ms / number of ms in 1s / number of s in 1min * 100% / this.slotDurationInMinutes
    return Math.floor((((slotEndDateTime.getTime() - slotStartDateTime.getTime()) / 1000 / 60) * 100) / this.slotDurationInMinutes);
  }

  getWidthPercentage(numberOfItems: number): number {
    return Math.floor(100 / numberOfItems);
  }

  getLeftPercentage(numberOfItems: number, index: number): number {
    return this.getWidthPercentage(numberOfItems) * index;
  }
}
